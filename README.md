
# Sons do Sul: Uma Visão Geral dos Estilos Musicais Regionais

Ciclo 4 - Momento Criação: o uso de tecnologias na autoria e publicação

Acesse aqui: <https://musica-unis.gitlab.io/ciclo-4-momento-criacao>

---

## Integrantes

Caroline  
Ettore Leandro Tognoli  
Fernando Morais  
Igor Lima  
Iura  
Malu Garcia  
Sabrina Andrade  

---

## Referências

<http://violaobrasil.com.br/glossario/o-que-e-estilo-xote/>

<http://pt.wikipedia.org/wiki/Xote>

<https://ritmosteclado.com/quais-sao-os-instrumentos-utilizados-no-xote/> <http://blogsouzalima.com.br/quais-sao-as-principais-caracteristicas-do->

Wikipedia - <https://pt.wikipedia.org/wiki/Fandango>

Portal São Francisco - <https://www.portalsaofrancisco.com.br/arte/fandango>

Spotify -  <https://open.spotify.com/artist/2TfLvUgSjqTUpfo0RCZHCb?si=XoTdH094Tje4a2uaOWvaLw>

Wikipedia - <https://pt.wikipedia.org/wiki/A_Sertaneja>

<http://luizmarenco.com.br/>

<https://www.letras.com.br/joao-luiz-correa/biografia>

Bueno, Eric Allen. "IDENTIDADE CULTURAL E SONHO DE PROGRESSO: UM PERCURSO DO FILME “CORAÇÃO DE LUTO”(1966) DO CANTOR TEIXEIRINHA CULTURAL IDENTITY AND DREAM OF PROGRESS: A TOUR OF THE FILM “HEART IN MOURNING”(1966) THE SINGER."

Cougo Júnior, Francisco Alcides. "Canta meu povo: uma interpretação histórica sobre a produção musical de Teixeirinha (1959-1985)." (2010).

Rossini, Miriam de Souza. "Por uma historiografia do cinema gaúcho: as pesquisas a partir da pós-graduação no RS." Congresso Brasileiro de Ciências da Comunicação (46.: 2023: Belo Horizonte, MG). Anais [recurso eletrônico]. São Paulo: Intercom, 2023. 2023.

Ferreira, Mauro. “Teixeirinha, popular cantor gaúcho dos anos 1960 e 1970, é reavivado em disco com 12 gravações inéditas”. Globo G1,  2020.

Google Maps
<https://maps.app.goo.gl/ntDkpAkhLu44WASv7>
