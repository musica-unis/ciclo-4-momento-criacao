---
marp: true
theme: default
---

<style>
section.center p, section.center ul, section.center li,
section.center h1,  section.center h2, section.center h3,
section.center h4,  section.center h5, section.center h6  {
    text-align: center;
}
section {
    text-align: left;
    font-size: 28px;
}
ul {
    margin-left: 0px;
}
section h1 {
    font-size: 40px;
}
section::after {
  font-weight: bold;
}
section.cover header{
    opacity: 0.2;
}
section.title h1 {
  font-size: 55px;
}
</style>

<!-- _class: invert cover title center -->
<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->

<!-- _footer: Powered by [Marp](https://yhatt.github.io/marp/)-->

## Ciclo 4 - Momento Criação: o uso de tecnologias na autoria e publicação

# Sons do Sul: Uma Visão Geral dos Estilos Musicais Regionais

---

# Integrantes

Caroline  
Ettore Leandro Tognoli  
Fernando Morais  
Igor Lima  
Iura  
Malu Garcia  
Sabrina Andrade  

---

# Sumário

- Balaio
- Xote
- Fandango
- Teixeirinha

---
<!-- header: "" -->
<!-- _class: center invert cover -->

# Balaio

---

<!-- header: "Balaio" -->

O balaio se apresenta como uma dança em grupos femininos e masculinos, em apresentação de sapateado e movimentos ágeis.

É muito comum no Rio Grande do Sul, podendo também ser conhecido como bambaquerê, mas sua origem foi no Nordeste brasileiro.

O nome balaio veio da semelhança da saia das moças que dançam com um cesto ao se movimentar.

---

<!-- header: "" -->
<!-- _class: center invert cover -->

Grande artista de Balaio

# Leandro Berlesi

![bg right:35% vertical](./resources/leandro-1.png)
![bg right](./resources/leandro-2.png)

---

![bg left:35%](./resources/leandro-3.png)

<!-- header: "Leandro Berlesi - Balaio" -->

Um artista contemporâneo de destaque na região Sul do país é Leandro Berlesi, cantor e compositor do regionalismo gaúcho com destaque para atuação no segmento de música e dança folclórica.
Natural de Tenente Portela - RS, já levou seu trabalho para quase todos os estados do Brasil, valendo também ressaltar sua performance em um baile gaúcho realizado na Times Square, NY -  Estados Unidos.
O que o coloca nesta menção é o destaque de sua gravação de “Balaio”, tradicionalíssimo tema gaúcho utilizado para a dança do Balaio nas quadrilhas e bailes gaúchos, que foi retirado de um trecho da peça instrumental “A Sertaneja” composta por Barbosa Lessa e Paixão Cortes.

---

<!-- header: "" -->
<!-- _class: center invert cover -->

# Xote

![bg right:35% vertical](./resources/xote-1.jpg)
![bg right](./resources/xote-3.jpg)

---

<!-- header: "Xote" -->

![bg left:35% vertical](./resources/xote-2.jpg)

O xote é um estilo que possui um ritmo de andamento médio para lento. Normalmente pautado em compassos dois por dois, com as tonalidades maiores e menores. Acento característico do ritmo ocorre no segundo pulso, primeira subdivisão. Ele possui uma instrumentação tradicional brasileiro, com o canto desempenhado por um instrumentista do grupo.

---

O estilo conta com instrumentos como acordeão, zabumba, triângulo, piano e violão. Sua estrutura composicional dá-se de forma binária, sob duas partes com apresentações da primeira seção. O xote possui alguns estilos, sendo eles: Xote-carreirinho: estilo comum no Paraná e Rio Grande do Sul, com coreografia próxima à da polca dançada pelos colonos alemães no Brasil, e tradicionalmente tocado com gaitas.

---

Xote-duas-damas: variante de xote, dançado do Rio Grande do Sul, na qual o cavalheiro dança acompanhado de duas damas.
Xote-bragantino: estilo popular no Pará, sua coreografia difere bastante da original.
Existem várias maneiras de dançar o xote:
Xote de Duas Damas: Semelhante ao estilo alemão, onde um trabalhador rural dança com duas damas.
Xote Carreirinha: Os casais correm na mesma direção, lembrando a ritsch-polka alemã.
Xote de Sete Voltas: O casal dá sete voltas pelo salão, alternando direções.
Xote do Chico Sapateado: Combina movimentos da polca com sapateado e toques das pontas dos dedos.

---

<!-- header: "" -->
<!-- _class: center invert cover -->

![bg right](./resources/luiz-1.png)

Grande artista de Xote

# Luiz Marenco

---

<!-- header: "Luiz Marenco - Xote" -->

![bg left:30%](./resources/luiz-2.png)

Natural de Porto Alegre/RS, nasceu no dia 22/12/1964 iniciando sua carreira profissional em 1988, quando começou a participar de festivais, movimento importante para a cultura e que lhe
rendeu grandes conquistas em âmbito regional.

---

1990 • Grava seu primeiro disco ao lado de seu parceiro e amigo Jayme Caetano Braun - "Luiz Marenco canta Jayme Caetano Braun".
1991 • O disco "Luiz Marenco canta Jayme Caetano Braun" o leva ao prêmio Sharp, hoje conhecido como Prêmio Tim.
1997 • Troféu Vitória melhor intérprete do ano. - 1997 Troféu Vitória melhor música do ano "Quando o Verso Vem Pras Casa", parceria com Gujo Teixeira.
1999 • Participa do programa Rio Grande do Sul, um século de história da RBS TV.

---

2001 • Prêmio Açorianos de melhor disco do ano "Enchendo os Olhos de Campo" em parceria com Gujo Teixeira.
2002 • Movimento Tradicionalista Gaúcho concede o selo de Qualidade, Autenticidade e Tradicionalidade previsto pelo PROJETO ISO TCHÊ para o CD "Luiz Marenco Ao Vivo.
2002 • Disco de Ouro "De Bota e Bombacha", com José Claudio Machado.
2003 • Disco de Ouro "Luiz Marenco - Ao Vivo Duplo".
2003 • Participa da minissérie da TV Globo "A Casa Das Sete Mulheres".

---

2004 • Disco de Platina "Luiz Marenco - Ao Vivo Duplo". - 2007 DVD de ouro "Todo o Meu Canto".
2008 • Recebe da Radio Gaúcha e Rádio Gaúcha Sat o Troféu Guri.
2009 • Grava o CD "Sensitivo" com os compositores, Evair Soarez Gomez, Fernando Soares e Juliano Gomes.
2010 • Grava "Andapago" em pareceria com seu grande amigo Jari Terres.
2013 • Começa a gravar "SUL" uma obra com poemas de Sérgio Carvalho Pereira.
2015 • Lança no Theatro São Pedro CD SUL
2016 • Recebe a Comenda Oneyde Bertussi na Cidade de Caxias do Sul.
2017 • Começa os preparativos dos 30 anos de carreira, com a gravação do DVD SUL e o DVD Luiz Marenco 30 anos.

---

<!-- header: "" -->
<!-- _class: center invert cover -->

# Fandango

![bg right:40%](./resources/fandango-1.png)

---

<!-- header: "Fandango" -->

![bg left:40%](./resources/fandango-2.png)

A palavra Fandango vem de fado Português e significa dança e canto tradicional e fatus em latim, que significa destino.

---

Estilo de música caracterizado por sua dança, com movimentos fortes e marcantes, com sua origem na Espanha, mas trazido pelos portugueses. Fandango é uma forma musical que é característica do folclore espanhol, tem uma variedade de formas definidas pela região de origem e tem em sua dança as formas do Flamenco. Seu ritmo começa lento e aumenta gradativamente em passos rítmicos mais rápidos.

---

No Fandango Gaúcho, os instrumentos utilizados são, principalmente, a gaita e o violão. A composição deste estilo é a dança, canto e os estalos de castanholas. Atualmente, além das castanholas, são também utilizados guitarras e pandeiros.

---

<!-- header: "" -->
<!-- _class: center invert cover -->

Grande artista de Fandango

# João Luiz Corrêa

![bg right:40%](./resources/joao-1.jpg)

---

<!-- header: "João Luiz Corrêa - Fandango" -->

João Luiz Corrêa é músico profissional e compositor desde 1989. Até 1998 atuou em diferentes grupos da música gaúcha, recolhendo ensinamentos de como é importante cultivar a alegria, a simplicidade e o carisma, qualidades marcantes deste ícone.

---

No final de 1998, por não encontrar identidade ao estilo que defendia, acabou por seguir sua própria carreira, com o objetivo de desempenhar a música gaúcha mais autêntica, de acordo com sua cultura, nata do Rio Grande do Sul. Iniciou carreira-solo com o nome João Luiz Corrêa & Vozes do Vento, mudando o nome em seguida para João Luiz Corrêa & Grupo Campeirismo.

---

A partir daí, seu trabalho tomou grande expressão. Em treze anos, lançou treze trabalhos gravados em CD e cinco em DVD. Foi premiado com quatro Discos de Ouro, um Disco de Platina e um DVD de Ouro, que tem a importante marca de ser o primeiro DVD de Ouro da música gaúcha. Em setembro de 2011, mais uma grande notícia veio coroar a história de João Luiz Corrêa & Grupo Campeirismo: O prêmio CD e DVD de Ouro do trabalho “10 Anos de Sucesso”, lançado em setembro de 2010.

---

Sucessos como “Um Bagual Corcoveador”, “Fandango Em Soledade”, “Pau Que Dá Cavaco”, “Gauchão de Apartamento”, “Quando Tapeia o Chapelão” dentre outros, coroam de êxito a carreira deste artista.

---

É na atualidade o nome mais expressivo do meio musical gaúcho com uma agenda que supera 220 shows por ano. Destaca-se por sua fidelidade à tradição gaúcha, por seu estilo fandangueiro e por seu carisma inconfundível, que a cada dia conquista mais fãs por onde passa.

---

<!-- header: "" -->
<!-- _class: center invert cover -->

Grande artista da Região:

# Teixeirinha

![bg right:35% vertical](./resources/teixeirinha.png)
![bg right](./resources/teixeirinha-estatua.png)

---

<!-- header: "Quem foi Teixeirinha" -->

Vitor Mateus Teixeira, conhecido artisticamente como Teixeirinha, nasceu em 1927 na cidade de Rolante, no Rio Grande do Sul. Desde jovem, demonstrou interesse pela música, especialmente pelas tradições gaúchas. Ao longo de sua carreira, Teixeirinha se tornou uma figura emblemática da música regionalista, conquistando o coração dos brasileiros com suas canções emotivas e autênticas. Sua identificação com a cultura do Sul e sua habilidade de transmitir os valores e tradições desta região o tornaram um ícone da música brasileira.

---

<!-- header: "Contribuições para a Música Brasileira" -->

Teixeirinha deixou um legado significativo na música do Brasil, especialmente no cenário regionalista. Sua abordagem única e suas composições sinceras e envolventes influenciaram profundamente a música popular brasileira. Ele foi um dos pioneiros a levar a música gaúcha para além das fronteiras do Rio Grande do Sul, alcançando sucesso em todo o país. Sua voz marcante e suas letras profundas tocaram o coração de milhões de pessoas, estabelecendo-o como um dos grandes nomes da música brasileira.

---

<!-- header: "Temáticas de Suas Canções" -->

As músicas de Teixeirinha abordam uma variedade de temas, refletindo sua vivência e sua conexão com a cultura gaúcha. Ele cantava sobre o amor, a saudade, a vida no campo, as tradições e os valores do Sul do Brasil. Suas letras emotivas e melódicas retratam experiências humanas universais, conquistando não apenas os gaúchos, mas pessoas de todo o país. Teixeirinha conseguia capturar a essência da vida no campo e transmitir essas emoções através de suas músicas de maneira cativante e autêntica.

---

<!-- header: "Popularidade e Legado" -->

Teixeirinha alcançou uma popularidade extraordinária ao longo de sua carreira, tornando-se um dos artistas mais queridos e respeitados do Brasil. Suas músicas conquistaram não apenas o público do Sul, mas também admiradores em todo o país. Seu legado perdura até os dias de hoje, com suas canções ainda sendo ouvidas e apreciadas por pessoas de todas as idades. Teixeirinha deixou uma marca indelével na música brasileira, inspirando gerações de artistas e continuando a encantar os ouvintes com sua música atemporal.

---

<!-- header: "" -->
<!-- _class: center invert cover -->

# FIM

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->
<!-- paginate: false -->
<!-- _class: center invert cover -->

Obrigado!
